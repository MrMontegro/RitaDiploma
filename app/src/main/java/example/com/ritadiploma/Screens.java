package example.com.ritadiploma;

/**
 * Created by aleks on 06.06.2017.
 */

public interface Screens {
    String SCREEN_DATA = "data";
    String SCREEN_CATALOGUE = "catalogue";
    String SCREEN_DOCS = "docs";
    String SCREEN_CLIENT = "client";
    String SCREEN_REQUEST = "request";
    String SCREEN_SERVICE = "service";
    String SCREEN_SPECIALIST = "specialist";
    String SCREEN_EMPLOYEE = "employee";
    String SCREEN_GOODS = "goods";
    String SCREEN_DOCUMENT = "document";
    String SCREEN_PROVIDER = "provider";
}

package example.com.ritadiploma;

import java.util.List;

import example.com.ritadiploma.model.Client;
import example.com.ritadiploma.model.Specialist;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface Service {
    @GET("webapi/specialists")
    Call<List<Specialist>> getSpecialists();

    @GET("webapi/clients")
    Call<List<Client>> getClients(@Query("name") String name);

    @POST("webapi/clients")
    Call<Client> addClient(@Body Client client);

    @DELETE("webapi/clients")
    Call<Integer> deleteClient(@Query("id") int id);

    @PUT("webapi/clients")
    Call<Client> updateClient(@Body Client client);
}

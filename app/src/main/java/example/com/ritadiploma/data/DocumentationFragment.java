package example.com.ritadiploma.data;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import example.com.ritadiploma.R;
import example.com.ritadiploma.data.fragment.DocsStaticticsFragment;
import example.com.ritadiploma.data.fragment.TeamStatFragment;


public class DocumentationFragment extends Fragment {




    public DocumentationFragment() {
        // Required empty public constructor
    }


    public static DocumentationFragment newInstance() {
        DocumentationFragment fragment = new DocumentationFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_documntation, container, false);
        Button b = (Button)v.findViewById(R.id.form_stat_butt);
        Button b1 = (Button)v.findViewById(R.id.form_team_stat_butt);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.flContent, DocsStaticticsFragment.newInstance()).addToBackStack("").commit();
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.flContent, TeamStatFragment.newInstance()).addToBackStack("").commit();
            }
        });
        return v;
    }

}

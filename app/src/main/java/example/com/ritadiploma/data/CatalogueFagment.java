package example.com.ritadiploma.data;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import example.com.ritadiploma.R;
import example.com.ritadiploma.data.adapter.CatalogueItemAdapter;
import example.com.ritadiploma.model.CatalogueItem;


public class CatalogueFagment extends Fragment {



    public CatalogueFagment() {
        // Required empty public constructor
    }

    public static CatalogueFagment newInstance() {
        CatalogueFagment fragment = new CatalogueFagment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_catalogue_fagment, container, false);
        ListView lv = (ListView) v.findViewById(R.id.listView_catalogue);
        List<CatalogueItem> list = new ArrayList<>();
        list.add(new CatalogueItem("ТОВ ТЕЛОС","Україна","Добрива","4756"));
        list.add(new CatalogueItem("ТОВ ТЕЛОС","Україна","Фарби","1478"));
        list.add(new CatalogueItem("ТОВ ТЕЛОС","Україна","Розчинники","12475"));
        list.add(new CatalogueItem("ТОВ Добробут","Україна","Концентрати","32651"));
        list.add(new CatalogueItem("ТОВ Добробут","Україна","Добрива","14569"));
        list.add(new CatalogueItem("ТОВ Добробут","Україна","Антиоксиданти","6587"));
        list.add(new CatalogueItem("ТОВ Добробут","Україна","Мінерали","8712"));
        list.add(new CatalogueItem("ТОВ Експерт","Україна","Розчинники","6541"));
        list.add(new CatalogueItem("ТОВ Експерт","Україна","Смоли","8514"));
        list.add(new CatalogueItem("ТОВ Експерт","Україна","Каучук","6528"));
        list.add(new CatalogueItem("ТОВ Дарниця","Україна","Базові масла","1472"));
        list.add(new CatalogueItem("ТОВ Дарниця","Україна","Кислоти","24103"));
        list.add(new CatalogueItem("ТОВ Дарниця","Україна","Фосфати","19634"));
        CatalogueItemAdapter catalogueItemAdapter = new CatalogueItemAdapter(getContext(),list);
        lv.setAdapter(catalogueItemAdapter);
        return v;
    }

}

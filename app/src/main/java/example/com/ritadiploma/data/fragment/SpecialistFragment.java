package example.com.ritadiploma.data.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import example.com.ritadiploma.R;

public class SpecialistFragment extends Fragment {

    public SpecialistFragment() {
        // Required empty public constructor
    }


    public static SpecialistFragment newInstance() {
        SpecialistFragment fragment = new SpecialistFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_specialist, container, false);
        return view;
    }

}

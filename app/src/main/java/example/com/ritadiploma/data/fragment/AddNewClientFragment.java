package example.com.ritadiploma.data.fragment;


import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import example.com.ritadiploma.R;
import example.com.ritadiploma.Service;
import example.com.ritadiploma.ServiceHolder;
import example.com.ritadiploma.model.Client;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddNewClientFragment extends Fragment {

    private Service service = ServiceHolder.getServiceInstance().getService();
    String button_text;
    Client client = new Client();

    public AddNewClientFragment() {

    }

    public static AddNewClientFragment newInstance(String button_text,Client client) {
        AddNewClientFragment fragment = new AddNewClientFragment();
        Bundle args = new Bundle();
        args.putString("button",button_text);
        if(client!=null) {
            args.putInt("id", client.getId());
            args.putString("address", client.getAddress());
            args.putString("email", client.getEmail());
            args.putString("name", client.getName());
            args.putString("status", client.getStatus());
            args.putInt("req", client.getRequestQuantity());
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            button_text=getArguments().getString("button");
            if(!getArguments().isEmpty()){
                client.setId(getArguments().getInt("id"));
                client.setAddress(getArguments().getString("address"));
                client.setStatus(getArguments().getString("status"));
                client.setName(getArguments().getString("name"));
                client.setEmail(getArguments().getString("email"));
                client.setRequestQuantity(getArguments().getInt("req"));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_new_client, container, false);
        final EditText et_address = (EditText)v.findViewById(R.id.et_client_address);
        final EditText et_email_ = (EditText)v.findViewById(R.id.et_client_email);
        final EditText et_name = (EditText)v.findViewById(R.id.et_client_name);
        final EditText et_rq = (EditText)v.findViewById(R.id.et_client_request_quantity);
        final EditText et_status = (EditText)v.findViewById(R.id.et_client_status);
        Button b = (Button) v.findViewById(R.id.add_client_butt);
        if(button_text.equals("Додати")) {
            b.setText("Додати");
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Client client1 = new Client();
                    client1.setAddress(et_address.getText().toString());
                    client1.setEmail(et_email_.getText().toString());
                    client1.setName(et_name.getText().toString());
                    client1.setRequestQuantity(Integer.parseInt(et_rq.getText().toString()));
                    client1.setStatus(et_status.getText().toString());
                    Call<Client> call = service.addClient(client1);
                    call.enqueue(new Callback<Client>() {
                        @Override
                        public void onResponse(Call<Client> call, Response<Client> response) {
                            Toast.makeText(getContext(), "Додано успішно", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        }

                        @Override
                        public void onFailure(Call<Client> call, Throwable t) {

                        }
                    });
                }
            });
        } else if( button_text.equals("Редагувати")){
            b.setText("Редагувати");
            et_address.setText(client.getAddress());
            et_email_.setText(client.getEmail());
            et_name.setText(client.getName());
            et_rq.setText(String.valueOf(client.getRequestQuantity()));
            et_status.setText(client.getStatus());
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    client.setAddress(et_address.getText().toString());
                    client.setEmail(et_email_.getText().toString());
                    client.setRequestQuantity(Integer.parseInt(et_rq.getText().toString()));
                    client.setName(et_name.getText().toString());
                    client.setStatus(et_status.getText().toString());
                    Call<Client> call = service.updateClient(client);
                    call.enqueue(new Callback<Client>() {
                        @Override
                        public void onResponse(Call<Client> call, Response<Client> response) {
                            Toast.makeText(getContext(), "Відредактовано", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        }

                        @Override
                        public void onFailure(Call<Client> call, Throwable t) {

                        }
                    });
                }
            });

        }
        return v;
    }

}

package example.com.ritadiploma.data;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import example.com.ritadiploma.R;
import example.com.ritadiploma.Screens;
import example.com.ritadiploma.data.fragment.ClientFragment;
import example.com.ritadiploma.data.fragment.DocumentFragment;
import example.com.ritadiploma.data.fragment.EmployeeFragment;
import example.com.ritadiploma.data.fragment.GoodsFragment;
import example.com.ritadiploma.data.fragment.ProviderFragment;
import example.com.ritadiploma.data.fragment.RequestFragment;
import example.com.ritadiploma.data.fragment.ServiceFragment;
import example.com.ritadiploma.data.fragment.SpecialistFragment;


public class DataFragment extends Fragment implements View.OnClickListener {

    Button client;
    Button zapit;
    Button posluga;
    Button specialist;
    Button pracivnic;
    Button tovar;
    Button document;
    Button postachalnik;


    public DataFragment() {
        // Required empty public constructor
    }


    public static DataFragment newInstance() {
        DataFragment fragment = new DataFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_vvedenna, container, false);
        client = (Button) v.findViewById(R.id.client);
        zapit = (Button) v.findViewById(R.id.request);
        posluga = (Button) v.findViewById(R.id.service);
        specialist = (Button) v.findViewById(R.id.specialist);
        pracivnic = (Button) v.findViewById(R.id.employee);
        tovar = (Button) v.findViewById(R.id.goods);
        document = (Button) v.findViewById(R.id.document);
        postachalnik = (Button) v.findViewById(R.id.provider);

        client.setOnClickListener(this);
        zapit.setOnClickListener(this);
        posluga.setOnClickListener(this);
        specialist.setOnClickListener(this);
        pracivnic.setOnClickListener(this);
        tovar.setOnClickListener(this);
        document.setOnClickListener(this);
        postachalnik.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        String str = "";
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (v.getId()) {
            case R.id.client: {
                str = Screens.SCREEN_CLIENT;
                fragmentClass = ClientFragment.class;
                break;
            }
            case R.id.request: {
                str = Screens.SCREEN_REQUEST;
                fragmentClass = RequestFragment.class;
                break;
            }
            case R.id.service: {
                str = Screens.SCREEN_SERVICE;
                fragmentClass = ServiceFragment.class;
                break;
            }
            case R.id.specialist: {
                str = Screens.SCREEN_SPECIALIST;
                fragmentClass = SpecialistFragment.class;
                break;
            }
            case R.id.employee: {
                str = Screens.SCREEN_EMPLOYEE;
                fragmentClass = EmployeeFragment.class;
                break;
            }
            case R.id.goods: {
                str = Screens.SCREEN_GOODS;
                fragmentClass = GoodsFragment.class;
                break;
            }
            case R.id.document: {
                str = Screens.SCREEN_DOCUMENT;
                fragmentClass = DocumentFragment.class;
                break;
            }
            case R.id.provider: {
                str = Screens.SCREEN_PROVIDER;
                fragmentClass = ProviderFragment.class;
                break;
            }

        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.flContent, fragment)
                .addToBackStack(str)
                .commit();
    }
}

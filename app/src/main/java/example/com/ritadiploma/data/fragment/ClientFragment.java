package example.com.ritadiploma.data.fragment;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import example.com.ritadiploma.R;
import example.com.ritadiploma.Service;
import example.com.ritadiploma.ServiceHolder;
import example.com.ritadiploma.data.adapter.ClientAdapter;
import example.com.ritadiploma.model.Client;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ClientFragment extends Fragment implements View.OnClickListener {

    private ListView listView;
    private FloatingActionButton fab;
    private ClientAdapter adapter;
    private List<Client> clients = new ArrayList<>();
    private Service service = ServiceHolder.getServiceInstance().getService();
    private Client client_to_change;
    int listPosition;

    public ClientFragment() {
        // Required empty public constructor
    }


    public static ClientFragment newInstance() {
        ClientFragment fragment = new ClientFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_client, container, false);
        setHasOptionsMenu(true);
        initializeViews(view);
        loadData("");
        return view;
    }

    private void loadData(String s) {
        clients.clear();
        adapter.notifyDataSetChanged();
        Call<List<Client>> call = service.getClients(s);
        call.enqueue(new Callback<List<Client>>() {
            @Override
            public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {
                if (response.isSuccessful()) {
                    clients.addAll(response.body());
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Client>> call, Throwable t) {
                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void initializeViews(View view) {
        listView = (ListView) view.findViewById(R.id.listView);
        adapter = new ClientAdapter(getContext(), clients);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);


        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadData(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                loadData("");
                return false;
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.listView) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            listPosition = info.position;
            client_to_change = (Client) listView.getItemAtPosition(listPosition);
            getActivity().getMenuInflater().inflate(R.menu.long_click_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.видалити:
                Call<Integer> call = service.deleteClient(client_to_change.getId());
                call.enqueue(new Callback<Integer>() {
                    @Override
                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                        Toast.makeText(getContext(), "Видалено", Toast.LENGTH_SHORT).show();
                        update();
                    }

                    @Override
                    public void onFailure(Call<Integer> call, Throwable t) {

                    }
                });
                break;
            case R.id.редагувати:
                getFragmentManager().beginTransaction().replace(R.id.flContent,AddNewClientFragment.newInstance("Редагувати",client_to_change)).addToBackStack("").commit();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.update) {
            update();
            return true;
        }
        return false;
    }

    private void update() {
        loadData("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab: {
                addNewClient();
                break;
            }
        }
    }

    private void addNewClient() {
        getFragmentManager().beginTransaction().replace(R.id.flContent,AddNewClientFragment.newInstance("Додати",new Client(0,"","",0,"",""))).addToBackStack("").commit();
    }
}

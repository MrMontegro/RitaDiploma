package example.com.ritadiploma.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import example.com.ritadiploma.R;
import example.com.ritadiploma.model.Client;

/**
 * Created by aleks on 06.06.2017.
 */

public class ClientAdapter extends ArrayAdapter<Client> {
    public ClientAdapter(@NonNull Context context, @NonNull List<Client> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Client client = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_client, null);
        }
        TextView idTextView = (TextView) convertView.findViewById(R.id.clientId);
        TextView addressTextView = (TextView) convertView.findViewById(R.id.clientAddress);
        TextView statusTextView = (TextView) convertView.findViewById(R.id.clientStatus);
        TextView quantityTextView = (TextView) convertView.findViewById(R.id.clientQuantity);
        TextView nameTextView = (TextView) convertView.findViewById(R.id.clientName);
        TextView emailTextView = (TextView) convertView.findViewById(R.id.clientEmail);
        idTextView.setText(String.valueOf(client.getId()));
        addressTextView.setText(client.getAddress());
        statusTextView.setText(client.getStatus());
        quantityTextView.setText(String.valueOf(client.getRequestQuantity()));
        nameTextView.setText(client.getName());
        emailTextView.setText(client.getEmail());
        return convertView;
    }
}

package example.com.ritadiploma.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import example.com.ritadiploma.R;
import example.com.ritadiploma.model.CatalogueItem;
import example.com.ritadiploma.model.Client;


public class CatalogueItemAdapter extends ArrayAdapter<CatalogueItem> {

    public CatalogueItemAdapter(@NonNull Context context, @NonNull List<CatalogueItem> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CatalogueItem catalogueItem = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.catalogue_item_layout, null);
        }
        TextView provider_name = (TextView) convertView.findViewById(R.id.providerName);
        TextView provider_country = (TextView) convertView.findViewById(R.id.providerCountry);
        TextView goods_name = (TextView) convertView.findViewById(R.id.goodsName);
        TextView goods_price = (TextView) convertView.findViewById(R.id.goodsPrice);
        provider_name.setText(catalogueItem.getProvider_name());
        provider_country.setText(catalogueItem.getProvider_country());
        goods_name.setText(catalogueItem.getGoods_name());
        goods_price.setText(catalogueItem.getGoods_price());
        return convertView;
    }

}

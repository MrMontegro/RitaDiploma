package example.com.ritadiploma;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aleks on 06.06.2017.
 */

public class ServiceHolder {
    private static ServiceHolder holder;
    private Service service;
    private Retrofit retrofit;

    private ServiceHolder() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(Service.class);
    }

    public static ServiceHolder getServiceInstance() {
        if (holder == null)
            holder = new ServiceHolder();
        return holder;
    }


    public Service getService() {
        return service;
    }
}

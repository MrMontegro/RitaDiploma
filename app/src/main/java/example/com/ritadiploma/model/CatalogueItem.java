package example.com.ritadiploma.model;



public class CatalogueItem {

    String provider_name;
    String provider_country;
    String goods_name;
    String goods_price;

    public CatalogueItem(String provider_name, String provider_country, String goods_name, String goods_price) {
        this.provider_name = provider_name;
        this.provider_country = provider_country;
        this.goods_name = goods_name;
        this.goods_price = goods_price;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public String getProvider_country() {
        return provider_country;
    }

    public void setProvider_country(String provider_country) {
        this.provider_country = provider_country;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(String goods_price) {
        this.goods_price = goods_price;
    }
}
